﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Data
{
    public class AssignmentRepository : IAssignmentRepository
    {
        private readonly AssignmentDbContext _assignmentDbContext;

        public AssignmentRepository(AssignmentDbContext assignmentDbContext)
        {
            _assignmentDbContext = assignmentDbContext;
        }

        public IEnumerable<Assignment> GetAllAssignments()
        {
            return _assignmentDbContext.Assignments.ToList();
        }

        public Assignment GetAssignment(int id)
        {
            return _assignmentDbContext.Assignments.Find(id);
        }

        public void AddAssignment(Assignment assignment)
        {
            _assignmentDbContext.Assignments.Add(assignment);
            _assignmentDbContext.SaveChanges();
        }

        public void EditAssignment(Assignment assignment)
        {
            Assignment _assignment = _assignmentDbContext.Assignments.Find(assignment.AssignmentId);
            _assignment.Title = assignment.Title;
            _assignment.Description = assignment.Description;
            _assignment.StartDate = assignment.StartDate;
            _assignment.EndDate = assignment.EndDate;
            _assignment.AssigneeId = assignment.AssigneeId;
            _assignment.PriorityId = assignment.PriorityId;
            _assignment.IsClosed = assignment.IsClosed;

            _assignmentDbContext.SaveChanges();
        }

        public void DeleteAssignment(int id)
        {
            Assignment assignment = _assignmentDbContext.Assignments.Find(id);
            _assignmentDbContext.Remove(assignment);
            _assignmentDbContext.SaveChanges();
        }
    }
}
