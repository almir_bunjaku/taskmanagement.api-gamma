﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Data
{
    public class PriorityRepository : IPriorityRepository
    {
        private readonly AssignmentDbContext _assignmentDbContext;

        public PriorityRepository(AssignmentDbContext assignmentDbContext)
        {
            _assignmentDbContext = assignmentDbContext;
        }

        public IEnumerable<Priority> GetAllPriorities()
        {
            return _assignmentDbContext.Priorities.ToList();
        }

        public Priority GetPriority(int id)
        {
            return _assignmentDbContext.Priorities.Find(id);
        }

        public void AddPriority(Priority priority)
        {
            _assignmentDbContext.Priorities.Add(priority);
            _assignmentDbContext.SaveChanges();
        }

        public void EditPriority(Priority priority)
        {
            Priority _priority = _assignmentDbContext.Priorities.Find(priority.PriorityId);
            _priority.PriorityName = priority.PriorityName;

            _assignmentDbContext.SaveChanges();
        }

        public void DeletePriority(int id)
        {
            Priority priority = _assignmentDbContext.Priorities.Find(id);
            _assignmentDbContext.Remove(priority);
            _assignmentDbContext.SaveChanges();
        }
    }
}
