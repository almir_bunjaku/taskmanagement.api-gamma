﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Data
{
    public interface IPriorityRepository
    {
        IEnumerable<Priority> GetAllPriorities();
        Priority GetPriority(int id);
        void AddPriority(Priority priority);
        void EditPriority(Priority priority);
        void DeletePriority(int id);

    }
}
