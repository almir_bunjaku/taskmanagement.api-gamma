﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Data
{
    public interface IAssigneeRepository
    {
        IEnumerable<Assignee> GetAllAssignees();
        Assignee GetAssignee(int id);
        void AddAssignee(Assignee assignee);
        void EditAssignee(Assignee assignee);
        void DeleteAssignee(int id);
    }
}
