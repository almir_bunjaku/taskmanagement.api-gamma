﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Data
{
    public class AssigneeRepository : IAssigneeRepository
    {
        private readonly AssignmentDbContext _assignmentDbContext;

        public AssigneeRepository(AssignmentDbContext assignmentDbContext)
        {
            _assignmentDbContext = assignmentDbContext;
        }

        public IEnumerable<Assignee> GetAllAssignees()
        {
            return _assignmentDbContext.Assignees.ToList();
        }

        public Assignee GetAssignee(int id)
        {
            return  _assignmentDbContext.Assignees.Find(id);
        }

        public void AddAssignee(Assignee assignee)
        {
            _assignmentDbContext.Assignees.Add(assignee);
            _assignmentDbContext.SaveChanges();
        }

        public void EditAssignee(Assignee assignee)
        {
            Assignee _assignee = _assignmentDbContext.Assignees.Find(assignee.AssigneeId);
            _assignee.FirstName = assignee.FirstName;
            _assignee.LastName = assignee.LastName;

            _assignmentDbContext.SaveChanges();
        }

        public void DeleteAssignee(int id)
        {
            Assignee assignee = _assignmentDbContext.Assignees.Find(id);
            _assignmentDbContext.Remove(assignee);
            _assignmentDbContext.SaveChanges();
        }
    }
}
