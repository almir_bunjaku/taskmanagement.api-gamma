﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Domain
{
    public interface IAssigneeService
    {
        IEnumerable<Assignee> GetAllAssignees();
        Assignee GetAssignee(int id);
        void AddAssignee(Assignee assignee);
        void EditAssignee(Assignee assignee);
        void DeleteAssignee(int id);
    }
}
