﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagementAPI_Gamma.Data;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Domain
{
    public class AssigneeService : IAssigneeService
    {
        private readonly IAssigneeRepository _assigneeRepository;

        public AssigneeService(IAssigneeRepository assigneeRepository)
        {
            _assigneeRepository = assigneeRepository;
        }

        public IEnumerable<Assignee> GetAllAssignees()
        {
            return _assigneeRepository.GetAllAssignees();
        }

        public Assignee GetAssignee(int id)
        {
            return _assigneeRepository.GetAssignee(id);
        }

        public void AddAssignee(Assignee assignee)
        {
            _assigneeRepository.AddAssignee(assignee);
        }

        public void EditAssignee(Assignee assignee)
        {
            _assigneeRepository.EditAssignee(assignee);
        }

        public void DeleteAssignee(int id)
        {
            _assigneeRepository.DeleteAssignee(id);
        }
    }
}
