﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Data;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Domain
{
    public class PriorityService : IPriorityService
    {
        private readonly IPriorityRepository _priorityRepository;

        public PriorityService(IPriorityRepository priorityRepository)
        {
            _priorityRepository = priorityRepository;
        }

        public IEnumerable<Priority> GetAllPriorities()
        {
            return _priorityRepository.GetAllPriorities();
        }

        public Priority GetPriority(int id)
        {
            return _priorityRepository.GetPriority(id);
        }

        public void AddPriority(Priority priority)
        {
            _priorityRepository.AddPriority(priority);
        }

        public void EditPriority(Priority priority)
        {
            _priorityRepository.EditPriority(priority);
        }

        public void DeletePriority(int id)
        {
            _priorityRepository.DeletePriority(id);
        }
    }
}
