﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Data;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Domain
{
    public class AssignmentService : IAssignmentService
    {
        private readonly IAssignmentRepository _assignmentRepository;

        public AssignmentService(IAssignmentRepository assignmentRepository)
        {
            _assignmentRepository = assignmentRepository;
        }

        public IEnumerable<Assignment> GetAllAssignments()
        {
            return _assignmentRepository.GetAllAssignments();
        }

        public Assignment GetAssignment(int id)
        {
            return _assignmentRepository.GetAssignment(id);
        }

        public void AddAssignment(Assignment assignment)
        {
            _assignmentRepository.AddAssignment(assignment);
        }

        public void EditAssignment(Assignment assignment)
        {
            _assignmentRepository.EditAssignment(assignment);
        }

        public void DeleteAssignment(int id)
        {
            _assignmentRepository.DeleteAssignment(id);
        }

    }
}
