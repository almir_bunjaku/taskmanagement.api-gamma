﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Domain
{
    public interface IAssignmentService
    {   
        IEnumerable<Assignment> GetAllAssignments();
        Assignment GetAssignment(int id);
        void AddAssignment(Assignment assignment);
        void EditAssignment(Assignment assignment);
        void DeleteAssignment(int id);
    }
}
