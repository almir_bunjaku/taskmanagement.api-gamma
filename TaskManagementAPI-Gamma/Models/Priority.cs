﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TaskManagementAPI_Gamma.Models
{
    public class Priority
    {
        public int PriorityId { get; set; }

        [Required]
        public String PriorityName { get; set; }
    }
}
