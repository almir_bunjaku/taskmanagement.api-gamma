﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManagementAPI_Gamma.Models
{
    public class Assignment
    {
        public int AssignmentId { get; set; }

        [Required]
        public String Title { get; set; }

        public String Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int AssigneeId { get; set; }

        public Assignee Assignee { get; set; }

        public int PriorityId { get; set; }

        public Priority Priority { get; set; }

        public Boolean IsClosed { get; set; }

    }
}
