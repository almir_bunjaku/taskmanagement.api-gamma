﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskManagementAPI_Gamma.Models;
using TaskManagementAPI_Gamma.Domain;
using System.Threading.Tasks;

namespace TaskManagementAPI_Gamma.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssignmentController : ControllerBase
    {
        private readonly IAssignmentService _assignmentService;

        public AssignmentController(IAssignmentService assignmentService)
        {
            _assignmentService = assignmentService;
        }

        [HttpGet]
        public IActionResult GetAllAssignments()
        {
            try
            {
                return Ok(_assignmentService.GetAllAssignments());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database.");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetAssignment(int id)
        {
            try
            {
                if (_assignmentService.GetAssignment(id) == null)
                {
                    return NotFound();
                }
                return Ok(_assignmentService.GetAssignment(id));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database.");
            }
        }

        [HttpPost]
        public IActionResult AddAssignment([FromBody] Assignment assignment)
        {
            try
            {
                if (assignment == null)
                {
                    return BadRequest();
                }
                _assignmentService.AddAssignment(assignment);
                return StatusCode(StatusCodes.Status201Created);
                
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error adding new assignment.");
            }
        }

        [HttpPut]
        public IActionResult EditAssignment(Assignment assignment)
        {
            try
            {
                if (_assignmentService.GetAssignment(assignment.AssignmentId) == null)
                {
                    return NotFound($"Assignment with ID = {assignment.AssignmentId} not found");
                }
                _assignmentService.EditAssignment(assignment);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error editing assignment.");
            }
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteAssignment(int id)
        {
            try
            {
                if (_assignmentService.GetAssignment(id) == null)
                {
                    return NotFound($"Assignment with ID = {id} not found");
                }
                _assignmentService.DeleteAssignment(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting assignment.");
            }
        }
    }
}