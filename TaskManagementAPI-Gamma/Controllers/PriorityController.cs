﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Domain;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PriorityController : ControllerBase
    {
        private readonly IPriorityService _priorityService;

        public PriorityController(IPriorityService priorityService)
        {
            _priorityService = priorityService;
        }

        [HttpGet]
        public IActionResult GetAllPriorities()
        {
            try
            {
                return Ok(_priorityService.GetAllPriorities());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database.");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetPriority(int id)
        {
            try
            {
                if (_priorityService.GetPriority(id) == null)
                {
                    return NotFound();
                }
                return Ok(_priorityService.GetPriority(id));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database.");
            }
        }

        [HttpPost]
        public IActionResult AddPriority([FromBody] Priority priority)
        {
            try
            {
                if (priority == null)
                {
                    return BadRequest();
                }
                _priorityService.AddPriority(priority);
                return StatusCode(StatusCodes.Status201Created);

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error adding new priority. ");
            }
        }

        [HttpPut]
        public IActionResult EditPriority(Priority priority)
        {
            try
            {
                if (_priorityService.GetPriority(priority.PriorityId) == null)
                {
                    return NotFound($"Priority with ID = {priority.PriorityId} not found");
                }
                _priorityService.EditPriority(priority);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error editing priority.");
            }
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeletePriority(int id)
        {
            try
            {
                if (_priorityService.GetPriority(id) == null)
                {
                    return NotFound($"Priority with ID = {id} not found");
                }
                _priorityService.DeletePriority(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting priority.");
            }
        }
    }
}
