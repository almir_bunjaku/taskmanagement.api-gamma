﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManagementAPI_Gamma.Domain;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssigneeController : ControllerBase
    {
        private readonly IAssigneeService _assigneeService;

        public AssigneeController(IAssigneeService assigneeService)
        {
            _assigneeService = assigneeService;
        }

        [HttpGet]
        public IActionResult GetAllAssignees()
        {
            try
            {               
                return Ok(_assigneeService.GetAllAssignees());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database.");
            }
        }

        [HttpGet("{id:int}")]
        public IActionResult GetAssignee(int id)
        {
            try
            {
                if (_assigneeService.GetAssignee(id) == null)
                {
                    return NotFound();
                }
                return Ok(_assigneeService.GetAssignee(id));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error retrieving data from the database.");
            }
        }

        [HttpPost]
        public IActionResult AddAssignee([FromBody] Assignee assignee)
        {
            try
            {
                if (assignee == null)
                {
                    return BadRequest();
                }
                _assigneeService.AddAssignee(assignee);
                return StatusCode(StatusCodes.Status201Created);

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error adding new assignee.");
            }
        }

        [HttpPut]
        public IActionResult EditAssignee(Assignee assignee)
        {
            try
            {
                if (_assigneeService.GetAssignee(assignee.AssigneeId) == null)
                {
                    return NotFound();
                }
                _assigneeService.EditAssignee(assignee);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error editing assignee.");
            }
        }

        [HttpDelete("{id:int}")]
        public IActionResult DeleteAssignee(int id)
        {
            try
            {
                if (_assigneeService.GetAssignee(id) == null)
                {
                    return NotFound($"Assignee with ID = {id} not found");
                }
               _assigneeService.DeleteAssignee(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error deleting assignee.");
            }
        }
    }
}
