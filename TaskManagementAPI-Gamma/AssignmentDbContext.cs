﻿using Microsoft.EntityFrameworkCore;
using TaskManagementAPI_Gamma.Models;

namespace TaskManagementAPI_Gamma
{
    public class AssignmentDbContext : DbContext
    {
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<Assignee> Assignees { get; set; }
        public DbSet<Priority> Priorities { get; set; }

        public AssignmentDbContext(DbContextOptions<AssignmentDbContext> options)
            : base(options)
        {

        }
    }
}
